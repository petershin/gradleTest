/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest


import org.gradle.testkit.runner.TaskOutcome
import org.ysb33r.gradle.gradletest.internal.GradleTestIntegrationSpecification
import spock.lang.Unroll

class TestGeneratorIntegrationSpec extends GradleTestIntegrationSpecification {

    File initGradle
    File generatedSourceDir

    void setup() {
        writeBuildScriptHeader()
        initGradle = new File(buildDir, 'gradleTest/init.gradle')
        generatedSourceDir = new File(projectDir, '.generated-src/gradleTestPlugin/gradleTest/src/groovy')
    }

    @Unroll
    void 'Source code is generated from template under Gradle #gradleVer'() {
        setup: "A project that contains a src/gradleTest layout"
        def tests = ['alpha', 'beta', 'gamma', 'delta', 'eta']

        createProjects(tests)

        buildFile << '''
            gradleTest {
                versions '3.999', '5.998'
                versions '4.997'
                kotlinDsl = true
            }
        '''

        when: 'The gradleTestGenerator task is executed'
        gradleRunner
            .withArguments('gradleTestGenerator', '--console=plain', '-i', '-s')
            .withGradleDistribution(new File(GRADLETESTREPO, "gradle-${gradleVer}-bin.zip").toURI())
            .withDebug(true)
            .build()

        def initScriptContent = initGradle.text

        then: 'Groovy+Spock test files are generated in the source set directory under the build directory'
        new File(generatedSourceDir, 'compatibilityTests/dsl/groovylang/AlphaSpec.groovy').exists()
        new File(generatedSourceDir, 'compatibilityTests/dsl/groovylang/BetaSpec.groovy').exists()
        new File(generatedSourceDir, 'compatibilityTests/dsl/kotlinlang/Beta_testTwoSpec.groovy').exists()
        new File(generatedSourceDir, 'compatibilityTests/dsl/groovylang/GammaSpec.groovy').exists()
        new File(generatedSourceDir, 'compatibilityTests/dsl/groovylang/EtaSpec.groovy').exists()
        new File(generatedSourceDir, 'compatibilityTests/dsl/kotlinlang/EtaSpec.groovy').exists()
        !new File(generatedSourceDir, 'compatibilityTests/dsl/kotlinlang/AlphaSpec.groovy').exists()

        and: 'Subdirectories without a build.gradle file will not be included'
        !new File(generatedSourceDir, 'compatibilityTests/dsl/groovylang/DeltaSpec.groovy').exists()

        and: 'An initscript is created'
        initScriptContent.contains("classpath fileTree ('${new File(buildDir, 'libs').toURI()}'.toURI())")
        initScriptContent.contains("dirs '${new File(buildDir, 'gradleTest/repo').toURI()}'.toURI()")

        when: 'The generated source file is inspected'
        String source = new File(generatedSourceDir, 'compatibilityTests/dsl/groovylang/AlphaSpec.groovy').text

        then:
        source.contains 'package compatibilityTests.dsl.groovylang'
        source.contains "result.task(':runGradleTest')"
        source.contains "void \"Alpha : Gradle #version using dsl.groovy\"()"
        source.contains "version << ['5.998','4.997']"

        where:
        gradleVer << allAvailableGradleReleases()
    }

    void 'When there is no gradleTest folder, the task should not fail, just be skipped'() {
        given: "There is no src/gradleTest folder and the plugin is applied"
        buildFile << '''
            gradleTest {
                versions '5.997'
            }
        '''

        when: 'The evaluation phase is completed'
        def result = gradleRunner
            .withArguments('gradleTestGenerator', '--console=plain', '-i', '-s')
            .withDebug(true)
            .build()

        then: 'The generator task should be skipped'
        result.task(':gradleTestGenerator').outcome == TaskOutcome.SKIPPED
    }

    @Unroll
    void 'Repositories can be injected into the init file under Gradle #gradleVer'() {
        given: 'There is no src/gradleTest folder and the plugin is applied'
        createProjects(['alpha'])

        buildFile << '''
            gradleTest {
                versions '5.997'
                
                initBuildscriptRepositoryBlock "maven {url '../local-repo'}"
            }
        '''

        when: 'The evaluation phase is completed'
        gradleRunner
            .withArguments('gradleTestGenerator', '--console=plain', '-i', '-s')
            .withGradleDistribution(new File(GRADLETESTREPO, "gradle-${gradleVer}-bin.zip").toURI())
            .withDebug(true)
            .build()

        def initScriptContent = initGradle.text

        then: 'The repository block is in the init script'
        initScriptContent.contains("maven {url")

        where:
        gradleVer << oneOfEachMainGradleReleases()
    }

    private void createProjects(List<String> tests) {
        File srcDir = new File(projectDir, 'src/gradleTest')
        srcDir.mkdirs()
        tests.each {
            File testDir = new File(srcDir, it)
            testDir.mkdirs()
            if (it != 'delta') {
                new File(testDir, 'build.gradle').text = ''
            }
            if (it == 'beta') {
                new File(testDir, 'testTwo.gradle.kts').text = ''
            }
            if (it == 'eta') {
                new File(testDir, 'build.gradle.kts').text = ''
            }
        }
    }
}
