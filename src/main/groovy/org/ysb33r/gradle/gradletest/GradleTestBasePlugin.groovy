/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest


import groovy.transform.CompileStatic
import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.util.GradleVersion
import org.ysb33r.gradle.gradletest.internal.Jacoco
import org.ysb33r.grolifant.api.core.LegacyLevel
import org.ysb33r.grolifant.api.core.ProjectOperations

/**
 * A base plugin for GradleTest. It provides only the ability to add new test sets,
 * but does not create any.
 *
 */
@CompileStatic
class GradleTestBasePlugin implements Plugin<Project> {

    public static final String SUPPORT_URL = 'https://gitlab.com/ysb33rOrg/gradleTest/-/issues'

    @Override
    void apply(Project project) {
        if (GradleVersion.current() < GradleVersion.version('4.3')) {
            throw new GradleException('This plugin is only compatible with Gradle 4.3+')
        }
        project.apply plugin: 'groovy'
        addTestKitTriggers(project)
        addJacocoTriggers(project)

        ProjectOperations.maybeCreateExtension(project)
    }

    /** Adds the appropriate task triggers.
     */
    private void addTestKitTriggers(Project project) {
        project.extensions.create(GradleTestSets.NAME, GradleTestSets, project)
    }

    private void addJacocoTriggers(Project project) {
        project.pluginManager.withPlugin('jacoco') {
            if(LegacyLevel.PRE_8_0) {
                project.tasks.withType(TestGenerator).configureEach { TestGenerator gt ->
                    Jacoco.configure(gt)
                }
            } else {
                project.tasks.whenTaskAdded { Task t ->
                    if(t instanceof AbstractGradleTest) {
                        Jacoco.configure((AbstractGradleTest)t)
                    }
                }
            }
        }
    }


}
