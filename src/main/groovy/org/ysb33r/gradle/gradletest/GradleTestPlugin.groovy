/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.plugin.devel.tasks.PluginUnderTestMetadata
import org.ysb33r.gradle.gradletest.internal.TestSet

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_5

/** Checks the current Gradle version and decides which real plugin to apply.
 *
 */
@CompileStatic
class GradleTestPlugin implements Plugin<Project> {

    public
    void apply(Project project) {
        applyTestKit(project)

        if (!PRE_4_5) {
            if (System.getProperty('gradleTest.include') || System.getProperty('gradleTest.exclude')) {
                project.logger.warn(
                    'gradleTest.(ex|in)clude system properties are no longer used on Gradle 4.4+. ' +
                        'Use --tests command-line parameter instead'
                )
            }
        }
    }

    private void applyTestKit(Project project) {
        project.with {
            apply plugin: GradleTestBasePlugin
        }
        TestSet.addTestSet(project, Names.DEFAULT_TASK)

        project.pluginManager.withPlugin('java-gradle-plugin') {
            project.tasks.all { Task t ->
                if (t instanceof AbstractGradleTest) {
                    def gt = (AbstractGradleTest) t
                    def putm = (PluginUnderTestMetadata) project.tasks.getByName('pluginUnderTestMetadata')
                    gt.classpath += putm.outputs.files
                    gt.dependsOn(putm)
                    gt.usePluginUnderTestMetadata = true
                }
                if (t instanceof ClasspathManifest) {
                    t.enabled = false
                }
            }
        }
    }

}
