/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.compile.GroovyCompile
import org.ysb33r.gradle.gradletest.AbstractGradleTest
import org.ysb33r.gradle.gradletest.ClasspathManifest
import org.ysb33r.gradle.gradletest.Names
import org.ysb33r.gradle.gradletest.TestGenerator

import static TestSet.SPOCK_V1_VERSION
import static TestSet.getSpockMode
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_5_0

/**
 * Utilities for configuration the GradleTest task.
 *
 * @author Schalk W. Cronjé
 *
 * @since 3.0
 */
@CompileStatic
class GradleTestTaskFamilyConfiguration {

    @CompileDynamic
    static SourceSetContainer getSourceSetContainer(Project project) {
        if (PRE_5_0) {
            (SourceSetContainer) project.sourceSets
        } else {
            project.extensions.getByType(SourceSetContainer)
        }
    }

    static void configureGradleTestTask(
        AbstractGradleTest testTask,
        SourceSet sourceSet,
        Object... dependsOnTasks
    ) {
        testTask.configure { AbstractGradleTest it ->
            it.group = Names.TASK_GROUP
            it.description = 'Runs Gradle compatibility tests'
            it.dependsOn dependsOnTasks
            it.mustRunAfter 'test'
            it.classpath = sourceSet.runtimeClasspath
        }
        setJunitMode(testTask)
        setClassesDirs(testTask, sourceSet)
    }

    static void configureGenTask(TestGenerator genTask) {
        genTask.identity {
            it.group = 'build'
            it.description = 'Creates text fixtures for compatibility testing'
        }
    }

    static void configureManifestTask(ClasspathManifest classpathManifest, String testSetBaseName) {
        classpathManifest.with {
            group = Names.TASK_GROUP
            description = 'Creates manifest file for ' + testSetBaseName
        }
    }

    static void configureGroovyCompileTask(GroovyCompile task, Object... dependsOnTasks) {
        task.with {
            dependsOn(dependsOnTasks)
            doFirst {
                destinationDir.deleteDir()
            }
        }
    }

    @CompileDynamic
    @TypeChecked
    private static void setJunitMode(AbstractGradleTest testTask) {
        if (spockMode == SPOCK_V1_VERSION) {
            testTask.configure { it.useJUnit() }
        } else {
            testTask.configure { it.useJUnitPlatform() }
        }
    }

    @CompileDynamic
    @TypeChecked
    private static void setClassesDirs(AbstractGradleTest testTask, SourceSet sourceSet) {
        testTask.configure {
            it.testClassesDirs = sourceSet.output.classesDirs
            it.inputs.files(sourceSet.output.classesDirs).skipWhenEmpty()
        }
    }
}
