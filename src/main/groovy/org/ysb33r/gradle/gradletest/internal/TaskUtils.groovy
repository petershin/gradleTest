/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileDynamic
import org.gradle.api.Task
import org.gradle.api.tasks.TaskContainer

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_5_0

class TaskUtils {

    @CompileDynamic
    static Task named(TaskContainer tasks, CharSequence name) {
        if (PRE_5_0) {
            tasks.getByName(name.toString())
        } else {
            tasks.named(name.toString()).get()
        }
    }

    @CompileDynamic
    static public <T> T named(TaskContainer tasks, CharSequence name, Class<? super T> type) {
        if (PRE_5_0) {
            tasks.withType(type).getByName(name.toString())
        } else {
            tasks.named(name.toString(), type).get()
        }
    }
}
