/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import org.gradle.api.Project
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.compile.GroovyCompile
import org.ysb33r.gradle.gradletest.AbstractGradleTest
import org.ysb33r.gradle.gradletest.ClasspathManifest
import org.ysb33r.gradle.gradletest.GradleTest
import org.ysb33r.gradle.gradletest.GradleTest43
import org.ysb33r.gradle.gradletest.Names
import org.ysb33r.gradle.gradletest.TestGenerator
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.core.TaskTools

import static org.gradle.language.base.plugins.LifecycleBasePlugin.CHECK_TASK_NAME
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.configureGenTask
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.configureGradleTestTask
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.configureGroovyCompileTask
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.configureManifestTask
import static org.ysb33r.gradle.gradletest.internal.GradleTestTaskFamilyConfiguration.getSourceSetContainer
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_5

/** Internal utility functions to add a new GradleTest test set.
 *
 * These methods should only be called during configuration phase.
 *
 * @since 1.0
 */
@CompileStatic
class TestSet {

    public final static String SPOCK_V2_VERSION = '2.0'
    public final static String SPOCK_V1_VERSION = '1.3'
    public final static String JUNIT_VERSION = '4.12'
    public final static String HAMCREST_VERSION = '1.3'

    /** Adds a test set with default dependencies
     *
     * @param project Project to add tests to
     * @param testSetName Name of test set
     */
    static void addTestSet(Project project, final String testSetName) {
        String setname = baseName(testSetName)
        project.configurations.maybeCreate setname
        addSourceSets(project, setname)
        addTestTasks(project, setname)
        addManifestTask(project, setname)
        addTestDependencies(project, setname)

        disableLicenseSourceChecks(project, setname)
    }

    /** Get a testset base name
     *
     * @param name Name to use as seed
     * @return Name that will be used as basename for configurations and tasks
     */
    static String baseName(final String testSetName) {
        testSetName == Names.DEFAULT_TASK ? Names.DEFAULT_TASK : "${testSetName}GradleTest"
    }

    /**
     * Provides the source file name for a test class.
     *
     * @param testName Testset name
     * @return Source file name
     *
     * @since 3.0
     */
    static String testClassSourceFileName(String testSetName) {
        "${testSetName.capitalize().replaceAll(/\W/, '_')}Spec.groovy"
    }

    /**
     * Creates a test filter from a full or partial test name.
     *
     * @param testSetName
     * @return Filter that is suitable to pass to {@link org.gradle.api.tasks.testing.AbstractTestTask} filter.
     *
     * @since 3.0
     */
    static String testClassFilter(String testSetName) {
        String base = testSetName.replaceAll(~/\W/) { List<String> it ->
            it[0] == '*' ? '*' : '_'
        }
        "**/${base}*".capitalize()
    }

    /** Gets the relative source dir where test code will be generated into.
     *
     * @param testSetBaseName A basename as returned by @link #baseName
     * @return A relative path to a root directory for source generation.
     */
    private
    static String getRelativeSourceDir(final String testSetBaseName) {
        final String postfix = "/${LANG_NAME}"
        ".generated-src/gradleTestPlugin/${testSetBaseName}/src${postfix}"
    }

    private
    static String getGeneratorTaskName(final String testSetBaseName) {
        "${testSetBaseName}${Names.GENERATOR_TASK_POSTFIX}"
    }

    private
    static String getLicenseTaskName(final String testSetBaseName) {
        "license${testSetBaseName.capitalize()}"
    }

    private
    static String getCodenarcTaskName(final String testSetBaseName) {
        "codenarc${testSetBaseName.capitalize()}"
    }

    private
    static String getTestTaskName(final String testSetBaseName) {
        testSetBaseName
    }

    static String getManifestTaskName(final String testSetBaseName) {
        "${testSetBaseName}${Names.MANIFEST_TASK_POSTFIX}"
    }

    /** Adds the required test dependencies to a configuration
     *
     * @param project
     * @param configurationName
     */
    private
    static void addTestDependencies(Project project, String testSetBaseName) {
        final SourceSet sourceSet = getSourceSetContainer(project).getByName(testSetBaseName)
        final String configurationName = sourceSet.implementationConfigurationName
        project.dependencies.with {
            add(configurationName, gradleTestKit())
            add(configurationName, gradleApi())
        }

        addSpockDependencyTo(project, configurationName)

        if (spockMode == SPOCK_V1_VERSION) {
            project.dependencies.with {
                add configurationName, "junit:junit:${JUNIT_VERSION}"
                add configurationName, "org.hamcrest:hamcrest-core:${HAMCREST_VERSION}"
            }
        }
    }

    private
    static void addSourceSets(Project project, final String testSetBaseName) {
        ProjectOperations po = ProjectOperations.find(project)

        SourceSet ss = getSourceSetContainer(project).create(testSetBaseName)
        SourceDirectorySet groovy = groovySourceDirectorySet(ss)

        groovy.with {
            srcDirs = [project.file(getRelativeSourceDir(testSetBaseName))]
        }

        ss.resources.with {
            srcDirs = [
                po.buildDirDescendant("${testSetBaseName}/resources")
            ]
        }
    }

    private
    static void addManifestTask(Project project, final String testSetBaseName) {
        registerManifestTask(ProjectOperations.find(project).tasks, testSetBaseName)
    }

    private
    static void addTestTasks(Project project, final String testSetBaseName) {
        final String genTaskName = getGeneratorTaskName(testSetBaseName)
        final String testTaskName = getTestTaskName(testSetBaseName)
        final String manifestTaskName = getManifestTaskName(testSetBaseName)
        final SourceSet sourceSet = getSourceSetContainer(project).getByName(testSetBaseName)
        final taskTools = ProjectOperations.find(project).tasks

        registerTestTasks(
            taskTools,
            sourceSet,
            testTaskName,
            genTaskName,
            manifestTaskName
        )
        configureCompileTask(
            taskTools,
            sourceSet.getCompileTaskName(LANG_NAME),
            genTaskName
        )
    }

    /**
     * Returns which version of Spock to use.
     *
     * @return {@link #SPOCK_V1_VERSION} or {@link #SPOCK_V2_VERSION}
     */
    static String getSpockMode() {
        final String gVer = GroovySystem.version
        if (gVer.startsWith('3.') || gVer.startsWith('2.5.')) {
            SPOCK_V2_VERSION
        } else if (gVer.startsWith('2.')) {
            SPOCK_V1_VERSION
        } else {
            SPOCK_V2_VERSION
        }
    }

    /** Creates a Spock Framework dependency
     *
     * @param project
     * @return
     */
    private
    @CompileDynamic
    static void addSpockDependencyTo(Project project, String cfgName) {
        final String spockVer = spockMode
        final String spockFullVer = "${spockVer}-groovy-${GroovySystem.version.replaceAll(/\.\d+(-SNAPSHOT)?$/, '')}"

        switch (spockVer) {
            case SPOCK_V1_VERSION:
                project.dependencies.add(cfgName, "org.spockframework:spock-core:${spockFullVer}") {
                    exclude module: 'groovy-all'
                }
                break
            default:
                project.dependencies.add(cfgName, "org.spockframework:spock-core:${spockFullVer}") {
                    exclude module: 'groovy'
                    transitive = true
                }
        }

    }

    /** Disables license checking if the Hierynomus License plugin is applied.
     *
     * @param project
     * @param testSetBaseName
     */
    private
    static void disableLicenseSourceChecks(Project project, final String testSetBaseName) {
        project.pluginManager.withPlugin('com.github.hierynomus.license') {
            project.tasks.getByName(getLicenseTaskName(testSetBaseName)).enabled = false
        }
    }

    private
    static void disableCodenarcSourceChecks(Project project, final String testSetBaseName) {
        project.pluginManager.withPlugin('codenarc') {
            project.tasks.getByName(getCodenarcTaskName(testSetBaseName)).enabled = false
        }
    }

    private static void registerTestTasks(
        TaskTools taskTools,
        SourceSet sourceSet,
        String testTaskName,
        String genTaskName,
        String manifestTaskName
    ) {
        taskTools.register(genTaskName, TestGenerator) {
            configureGenTask(it)
        }

        if (PRE_4_5) {
            taskTools.register(testTaskName, GradleTest43) { AbstractGradleTest it ->
                configureGradleTestTask(it, sourceSet, genTaskName, sourceSet.getCompileTaskName(LANG_NAME), manifestTaskName)
            }
        } else {
            taskTools.register(testTaskName, GradleTest) { AbstractGradleTest it ->
                configureGradleTestTask(it, sourceSet, genTaskName, sourceSet.getCompileTaskName(LANG_NAME), manifestTaskName)
            }
        }

        taskTools.whenNamed(CHECK_TASK_NAME) {
            it.dependsOn(testTaskName)
        }
    }

    @TypeChecked
    @CompileDynamic
    private static registerManifestTask(TaskTools tasks, String testSetBaseName) {
        tasks.register(getManifestTaskName(testSetBaseName), ClasspathManifest) {
            configureManifestTask(it, testSetBaseName)
        }
    }

    @CompileDynamic
    @TypeChecked
    private static configureCompileTask(
        TaskTools tasks,
        String compileTaskName,
        String genTaskName
    ) {
        tasks.whenNamed(compileTaskName, GroovyCompile) {
            configureGroovyCompileTask(it, genTaskName)
        }
    }

    @CompileDynamic
    private static SourceDirectorySet groovySourceDirectorySet(SourceSet ss) {
        ss.groovy
    }


//    private static createManifestTask(TaskContainer tasks, String testSetBaseName) {
//        ClasspathManifest task = tasks.create(getManifestTaskName(testSetBaseName), ClasspathManifest)
//        configureManifestTask(task, testSetBaseName)
//    }

    private static final String LANG_NAME = 'groovy'
}
