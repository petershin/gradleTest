/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.ysb33r.grolifant.api.core.ProjectOperations

/** Generates test files that will be compiled against GradleTestKit.
 *
 * @since 1.0
 */
@CompileStatic
class TestGenerator extends AbstractTestGenerator {

    TestGenerator() {
        def pt = ProjectOperations.find(project).providerTools
        this.testSourceDirProvider = linkedTestTaskProvider.map { AbstractGradleTest t ->
            t.testRootDirectory.get()
        }
        this.testMapProvider = testSourceDirProvider.map { File root ->
            buildTestMapFromDirectory(root)
        }
        onlyIf { getTestRootDirectory().get().exists() }
    }

    /** The root directory where to find tests for this specific GradleTest grouping
     * The default root directory by convention is {@code src/gradleTest}. THe patterns for the
     * directory is {@code src/} + {@code gradleTestSetName}.
     *
     * @return The directory as a file object resolved as per {@code project.file ( )}.
     */
    @InputDirectory
    Provider<File> getTestRootDirectory() {
        this.testSourceDirProvider
    }

    /** A map of the tests found in the appropriate GradleTest directory.
     * The default root directory by convention. See @link #getTestRootDirectory().
     * Locates all folder below the root which contains a {@code build.gradle} file -
     * other folders are ignored. If {@code getKotlinDsl ( )} returns {@code true}, then
     * folders containing {@code build.gradle.kts} will also be included
     *
     * @return A map of consisting of {@code <TestName,PathToTest>}.
     */
    @Input
    Provider<Map<String, TestDefinition>> getTestMap() {
        this.testMapProvider
    }

    /**
     * Builds a test map
     *
     * @return A test map
     *
     * @since 3.0
     */
    @Override
    protected Map<String, TestDefinition> buildTestMap() {
        testMapProvider.get()
    }

    private final Provider<File> testSourceDirProvider
    private final Provider<Map<String, TestDefinition>> testMapProvider
}
