/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic
import org.gradle.api.GradleException
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.InputDirectory
import org.ysb33r.gradle.gradletest.internal.ConfigurationCacheMode
import org.ysb33r.gradle.gradletest.internal.GradleVersions
import org.ysb33r.gradle.gradletest.internal.TestKitLocations
import org.ysb33r.grolifant.api.core.LegacyLevel
import org.ysb33r.grolifant.api.core.ProjectOperations

import java.util.concurrent.Callable

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_5

/**
 * Runs compatibility tests using special compiled GradleTestKit-based tests on Gradle 4.0-4.3
 *
 * @author Schalk W. Cronjé
 *
 * @since 3.0
 */
@CompileStatic
class GradleTest43 extends AbstractGradleTest {

    public static final TestKitLocations discardData = TestKitLocations.TEMPORARY
    public static final TestKitLocations directoryPerGroup = TestKitLocations.PER_GROUP
    public static final TestKitLocations directoryPerTest = TestKitLocations.PER_TEST

    public static final ConfigurationCacheMode configurationCacheOff = ConfigurationCacheMode.NONE
    public static final ConfigurationCacheMode configurationCacheWarn = ConfigurationCacheMode.WARN
    public static final ConfigurationCacheMode configurationCacheFail = ConfigurationCacheMode.FAIL

    GradleTest43() {
        super()

        if (!PRE_4_5) {
            throw new GradleException('This task type is only suitable for Gradle 4.3-4.4. Use GradleTest instead.')
        }

        if(System.getProperty("${name}.exclude")) {
            logger.warn("System property ${name}.exclude will be ignored." +
                "On Gradle 4.3-4.4 on ${name}.include is accepted for backwards compatibility.")
        }

        String[] include = System.getProperty("${name}.include")?.split(',')

        if(include) {
            setTestNameIncludePatterns(include.toList())
        }

        root = project.provider(new Callable<File>() {
            @Override
            File call() throws Exception {
                grolifant.fsOperations.file(rootFile)
            }
        })

        rootFile = "src/${name}"
    }

    /** The root directory where to find tests for this specific GradleTest grouping
     * The default root directory by convention is {@code src/gradleTest}. THe patterns for the
     * directory is {@code src/} + {@code gradleTestSetName}.
     *
     * @return The directory as a file object resolved as per {@code project.file ( )}.
     */
    @Override
    Provider<File> getTestRootDirectory() {
        this.root
    }

    /**
     * Overrides the default location of test project sources.
     *
     * @param src The location of the gradle test project sources.
     *
     * @since 3.0
     */
    @Override
    void setTestRootDirectory(Object src) {
        rootFile = src
    }

    private Object rootFile
    private final Provider<File> root
}

