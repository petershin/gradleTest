/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2015 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 * ============================================================================
 */
package org.ysb33r.gradle.gradletest

import groovy.transform.CompileStatic

/** Describes Gradle language DSLs.
 *
 * @since 2.0
 */
@CompileStatic
enum GradleScriptLanguage {
    GROOVY('groovy', 'gradle'),
    KOTLIN('kotlin', 'gradle.kts')

    final String id
    final String language
    final String buildFilePattern
    final String buildExtPattern
    final String settingsFilePattern

    private GradleScriptLanguage(final String lang, final String pat) {

        this.language = lang
        this.id = "dsl.${lang}"
        this.buildFilePattern = "build.${pat}"
        this.settingsFilePattern = "settings.${pat}"
        this.buildExtPattern = ".${pat}"
    }
}
