plugins {
    `java-gradle-plugin`
    id("org.ysb33r.gradletest")
    groovy
}

group = "my"
version = "1.0"

apply(from=System.getProperty("OFFLINE_REPO_DSL"))

tasks {

    named<org.ysb33r.gradle.gradletest.GradleTest>("gradleTest") {
        versions ("4.1","6.9")
    }

    val runGradleTest by creating {
        dependsOn ("gradleTest")
    }
}
