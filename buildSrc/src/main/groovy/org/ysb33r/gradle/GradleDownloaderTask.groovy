package org.ysb33r.gradle

import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.provider.Provider

@CompileStatic
class GradleDownloaderTask extends DefaultTask {

    final List<Provider<File>> distributions = []

    GradleDownloaderTask() {
        group = 'Verification'
        description = 'Download all Required Gradle distributions'
    }
}
