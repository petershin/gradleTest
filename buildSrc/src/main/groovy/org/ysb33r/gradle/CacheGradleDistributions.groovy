package org.ysb33r.gradle

import fi.linuxbox.gradle.download.Download
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Transformer
import org.gradle.api.file.RegularFile
import org.gradle.api.provider.Provider

@CompileStatic
class CacheGradleDistributions implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.apply plugin: 'fi.linuxbox.download'

        def destDir = project.layout.buildDirectory.file('gradleDistributionsUnderTest')

        def integrationTestSourceFile = project.file('src/integrationTest/groovy/org/ysb33r/gradle/gradletest/internal/GradleTestIntegrationSpecification.groovy')
        def integrationTestDistros = (integrationTestSourceFile.readLines().grep {
            it =~ /static\s+final\s+List\s+AVAILABLE_GRADLE_VERSIONS\s+=/
        })[0]
            .replaceFirst(~/^(.+?\[)/, '').replaceFirst(~/(\].*)$/, '')
            .split(',')
            .collect { it.trim().replaceAll(~/['"]/, '') }

        def downloader = project.tasks.register('downloadGradleDistributions', GradleDownloaderTask)

        integrationTestDistros.each { String dotDistro ->
            String snakeDistro = dotDistro.replaceAll(~/\./, '_')
            def downloadTask = project.tasks.register("downloadGradle${snakeDistro}", Download)
            def dest = destDir.map({ RegularFile it -> new File(it.asFile, "gradle-${dotDistro}-bin.zip") } as Transformer<File, RegularFile>)
            downloadTask.configure({ Download it ->
                it.description = "Caches Gradle ${dotDistro} for testing purposes"
                it.from.set("https://services.gradle.org/distributions/gradle-${dotDistro}-bin.zip".toString())
                it.to.fileProvider(dest)
                it.outputs.upToDateWhen { dest.get().exists() }
            } as Action<Download>)
            downloader.configure( { GradleDownloaderTask it ->
                it.dependsOn(downloadTask)
                it.distributions.add(dest)
            } as Action<GradleDownloaderTask>)

        }
    }


}
